﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CanDoUI_DATAGRID_ROWDRAG.aspx.cs"
    Inherits="GridDrag.CanDoUI_DATAGRID_ROWDRAG" %>

<!DOCTYPE html>
<html>
<head id="Head1" runat="server">
    <title></title>
    <link href="content/shared/styles/examples-offline.css" rel="stylesheet">
    <link href="styles/kendo.common.min.css" rel="stylesheet">
    <link href="styles/kendo.default.min.css" rel="stylesheet">
    <script src="js/jquery.min.js"></script>
    <script src="js/kendo.web.min.js"></script>
    <script src="content/shared/js/console.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="example" class="k-content">
        <asp:GridView ID="GridView1" runat="server" OnRowDeleting="GridView1_RowDeleting">
        </asp:GridView>
        <script>
            function attachKendoDragDrop() {
                $("#GridView1 tbody tr").kendoDraggable({
                    hint: function (element) {
                        var helperObj = $('<div style="width:100%;background:url(Styles/textures/highlight.pn) repeat"><table style="width:100%" cellspacing=0></table></div>').find('table').append($($(element).closest('tr').clone())).end();
                        helperObj.find('tr').attr("class", "k-alt ui-draggable ui-droppable").addClass("k-state-selected");
                        $($(element).closest('tr')).addClass("DragRow");
                        return helperObj;
                    },
                    dragstart: draggableOnDragStart,
                    drag: draggableOnDrag,
                    dragend: draggableOnDragEnd,
                    dragcancel: draggableOnCancel,
                    axis: 'y',
                    cursorOffset: { top: 4, left: 2 }
                });

                $("#GridView1").kendoDropTarget({
                    dragenter: droptargetOnDragEnter,
                    dragleave: droptargetOnDragLeave,
                    drop: droptargetOnDrop
                });
            }

            function onChange(arg) {
                var selected = $.map(this.select(), function (item) {
                    return $(item).text();
                });
                kendoConsole.log("Selected: " + selected.length + " item(s), [" + selected.join(", ") + "]");
            }

            function onDataBound(arg) {
                attachKendoDragDrop()
                kendoConsole.log("Grid data bound");
            }

            $(document).ready(function () {
                $("#GridView1").kendoGrid({
                    dataSource: {
                        pageSize: 10
                    },
                    change: onChange,
                    dataBound: onDataBound,
                    selectable: "single row",
                    pageable: true,
                    scrollable: false
                });
            });
        </script>
        <script>
            function draggableOnDragStart(e) {
                var rowIndex = $('#GridView1 tr').index($(e.target).closest('tr'));
                var Sno = $($($(e.target).closest('tr')).find("td:first-child")).text();
                kendoConsole.log("drag Start Sno:" + Sno);
            }

            function draggableOnDrag(e) {
                $($(".DragRow")).removeAttr("DragRow");
            }

            function draggableOnCancel(e) {
                kendoConsole.log("drag cancel");
                $($(".DragRow")).removeAttr("DragRow");
            }

            function draggableOnDragEnd(e) {
                kendoConsole.log("dragend");
                $($(".DragRow")).removeClass("DragRow");
            }

            function droptargetOnDragEnter(e) {
            }

            function droptargetOnDragLeave(e) {
                $($(".DragRow")).removeClass("DragRow");
            }

            function droptargetOnDrop(e) {
                var Sno = $($($(".DragRow").closest('tr')).find("td:first-child")).text();

                var DropNotAllowedRows = [2, 3, 5, 6, 7, 12, 15, 17, 19, 22, 24];

                if (("," + DropNotAllowedRows.join(",") + ",").indexOf("," + Sno + ",") != -1) {
                    $($(e.target).closest('tr')).after($(".DragRow"));
                    kendoConsole.log("Row Moved.");
                }
                else {
                    kendoConsole.log("<span style='color:red'>Row Drop Not Allowed. Sno:" + Sno);
                }

                $($(".DragRow")).removeClass("DragRow");

                //Reset The Row Styles
                $("#GridView1 tr:odd").removeAttr("class").removeAttr("style");
                $("#GridView1 tr:even").attr("class", "k-alt").removeAttr("style");
            }

            $(document).ready(function () {
                attachKendoDragDrop();
            });
            </script>
    </div>
    <div class="console">
    </div>
    </form>
</body>
</html>