﻿using System;
using System.Data.SqlClient;
using System.Web.UI.WebControls;

namespace GridDrag
{
    public partial class CanDoUI_DATAGRID_ROWDRAG : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string strSQLconnection = (@"Data Source=stack;Initial Catalog=artgallery;Persist Security Info=True;User ID=sa;Password=stack");

            SqlConnection sqlConnection = new SqlConnection(strSQLconnection);

            SqlCommand sqlCommand = new SqlCommand("select Sno,[Page No],[Trading Name],[Item Name],Association,Slot,Ref,Size from Trading_Lst", sqlConnection);
            try
            {
                sqlConnection.Open();

                SqlDataReader reader = sqlCommand.ExecuteReader();

                GridView1.DataSource = reader;

                GridView1.DataBind();
                //GridView1.Columns[0].Visible = false;//("Sno")
            }
            finally
            {
                sqlConnection.Close();
            }
        }

        protected void GridView1_RowDeleting(Object Sender, GridViewDeleteEventArgs e)
        {
        }
    }
}